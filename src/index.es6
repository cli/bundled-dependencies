#! /usr/bin/env node

const argv = require('minimist')(process.argv.slice(2));
const sh = require('shelljs');
const path = require('path');
const as_array = require("as/array");
const extend = require("extend");
const package_json = JSON.parse( sh.cat(path.resolve('./package.json')) );
const bundledDependencies = as_array( extend({}, package_json.dependencies, package_json.devDependencies) ).map(i=>i.key)
package_json.bundledDependencies = bundledDependencies;

if(argv.save === true){
  JSON.stringify( package_json, null, '  ' ).to('package.json');
}else{
  console.log( JSON.stringify( package_json, null, '  ') );
}
